# Service Catalog

Provides functionality to implement a service catalog in Drupal.


## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)


## Requirements

No special requirements.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules][installing_modules].


## Configuration


The module has no menu or modifiable settings. There is no configuration yet.

[installing_modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules
